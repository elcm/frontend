FROM node:current-alpine

WORKDIR /code
RUN npm install --no-cache angular karma typescript
RUN adduser --home /code -D developer && adduser developer users
USER developer:users

CMD ["ng", "serve"]

